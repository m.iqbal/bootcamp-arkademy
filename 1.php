<?php
	if (!isset($data)) $data = new stdClass();

	$data->name = "Muhammad Iqbal Arfandi";
	$data->age = 23;
	$data->address = "Yogyakarta";
	$data->hobbies = array('Basket', 'Badminton', 'Game' );
	$data->is_married = false;

	$list_school = [
	    [
	      "name"   => "MI Cokroaminoto Bandingan",
	      "year_in" => "2002",
	      "year_out"=> "2008",
	      "major" => null
	    ],
	    [
	      "name"   => "MtsN 1 Banjarnegara",
	      "year_in" => "2008",
	      "year_out"=> "2011",
	      "major" => null
	    ],
	    [
	      "name"   => "SMK N 2 Bawang",
	      "year_in" => "2011",
	      "year_out"=> "2014",
	      "major" => null
	    ],
	    [
	      "name"   => "Universitas Islam Indonesia",
	      "year_in" => "2014",
	      "year_out"=> "2019",
	      "major" => "Teknik Informatika"
	    ]
	];

	$data->list_school = $list_school;

	$skills = [
	    [
	      "skill_name"   => "PHP",
	      "level" => "advanced",
	    ],
	    [
	      "skill_name"   => "HTML",
	      "level" => "advanced",
	    ],
	    [
	      "skill_name"   => "Codeigniter",
	      "level" => "beginer",
	    ],
	];

	$data->skills = $skills;
	$data->interest_in_coding = true;

	$dataJSON = json_encode($data);

	echo $dataJSON;
?>